<?php

/**
 * @file It contains functions mail related
 *
 * Send all nodes suscribed by a user
 * All nodes belongs to same user
 * @param  array  $notifications nodes
 * @return send a digest message
 */
function _notifications_lite_sendmail($notifications = array()) {
  $body_comments = NULL;
  $body_nodes = NULL;
  if (!empty($notifications)) {

    if (isset($notifications['nodes'])) {
      foreach ($notifications['nodes'] as $notification_node) {
        $args_nodes[] = $notification_node->nid;
        $recipient = $notification_node->recipient;
        $uid = $notification_node->subscription->uid;
        $mode = $notification_node->subscription->mode;
        $format = $notification_node->subscription->format;
      }
      $body_nodes = views_embed_view('notifications_lite_nodes', 'default', implode(',', $args_nodes));
    }

    if (isset($notifications['comments'])) {
      foreach ($notifications['comments'] as $notification_comment) {
        $args_comments[] = $notification_comment->cid;
        $recipient = $notification_comment->recipient;
        $uid = $notification_comment->subscription->uid;
        $mode = $notification_comment->subscription->mode;
        $format = $notification_comment->subscription->format;
      }
      $body_comments = views_embed_view('notifications_lite_comments', 'default', implode(',', $args_comments));
    }

    $body = $body_nodes . $body_comments;
    $from = variable_get('site_mail', '');
    $language = language_default();
    $send = TRUE;
    $subject = t('The Mountain Forum Digest');
    $result = drupal_mail('notifications_lite', 'notifications_lite_digest_message', $recipient, $language, array('message' => $body, 'subject' => $subject, 'uid' => $uid, 'format' => $format), $from, $send);

    if ($result) {
      $record = array(
        'recipient' => $recipient,
        'mode' => $mode,
        'content' => $body,
        'format' => $format,
        'language' => 'en',
        'created' => time(),
      );
      db_insert('notifications_lite_log')->fields($record)->execute();
    }
  }
  else {
    watchdog('notifications_lite', 'Nothing to sending at @time', array('@time' => time()), WATCHDOG_NOTICE);
  }
}

/**
 * Implementation of hook_mail()
 */

function notifications_lite_mail($key, &$message, $params) {
  global $user;
  global $base_url;

  $options = array(
    'langcode' => $message['language']->language,
  );

  switch ($key) {
    case 'notifications_lite_digest_message':
      $format = notifications_lite_mail_header($params['format']);
      $filter = notifications_lite_mail_filter($params['format']);
      $message['headers'] = array(
        'MIME-Version' => '1.0',
        'Content-Type' => $format . ';charset=utf-8',
      );
      $message['subject'] = $params['subject'];
      $message['body'][] = t('@name sent you the following news digest:', array('@name' => 'The Mountain Forum team editor'));
      $message['body'][] = check_markup('<br />', $filter);
      $message['body'][] = check_markup($params['message'], $filter);
      $message['body'][] = check_markup('<br />', $filter);
      $message['body'][] = '--------------------';
      $message['body'][] = check_markup('<br />', $filter);
      $message['body'][] = t('Manage your subscriptions @here', array('@here' => $base_url . '/user/' . $params['uid']));
      $message['body'][] = check_markup('<p></p>', $filter);
      $message['body'][] = t('The Mountain Forum team editor');
      break;
  }
}

/**
 * Return header
 */

function notifications_lite_mail_header($format) {
  $formats = array(
    'full_html' => 'text/html',
    'plain_text' => 'text/plain',
  );
  return $formats[$format];
}


/**
 * Return filter
 */

function notifications_lite_mail_filter($format) {
  $formats = array(
    'full_html' => 'notifications_lite_full_html',
    'plain_text' => 'filtered_html',
  );
  return $formats[$format];
}
