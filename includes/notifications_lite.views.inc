<?php
function notifications_lite_views_data() {

  $data['notifications_lite_log']['table']['group'] = t('Notifications lite');

  $data['notifications_lite_log']['table']['base'] = array(
    'field' => 'id', // This is the identifier field for the view.
    'title' => t('Notifications lite'),
    'help' => t('Display notifications sent'),
    'weight' => -10,
  );

  $data['notifications_lite_log']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'recipient',
    ),
  );

  // Node ID table field.
  $data['notifications_lite_log']['id'] = array(
    'title' => t('Notifications id'),
    'help' => t('Display notification id'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
  );

  $data['notifications_lite_log']['recipient'] = array(
    'title' => t('E-mail recipient'),
    'help' => t('Display E-mail recipient'),
    // Define a relationship to the {node} table, so example_table views can
    // add a relationship to nodes. If you want to define a relationship the
    // other direction, use hook_views_data_alter(), or use the 'implicit' join
    // method described above.
    'relationship' => array(
      'base' => 'users', // The name of the table to join with.
      'base field' => 'mail', // The name of the field on the joined table.
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Username'),
      'title' => t('Username'),
      'help' => t('Display username'),
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['notifications_lite_log']['mode'] = array(
    'title' => t('Mode'),
    'help' => t('Display Alert or Digest mode'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['notifications_lite_log']['content'] = array(
    'title' => t('Content'),
    'help' => t('Display content send inside body mail'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['notifications_lite_log']['format'] = array(
    'title' => t('Format'),
    'help' => t('Display HTML or Plain text format'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['notifications_lite_log']['language'] = array(
    'title' => t('Language'),
    'help' => t('Display language'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example timestamp field.
  $data['notifications_lite_log']['created'] = array(
    'title' => t('Sent date'),
    'help' => t('Display sent date'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
