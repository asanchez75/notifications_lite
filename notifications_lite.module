<?php
/**
 * @file It contains hooks and important functions
 */

module_load_include('inc', 'notifications_lite', 'notifications_lite.admin');
module_load_include('inc', 'notifications_lite', 'notifications_lite.mail');

/**
 * Return intervals for function defined in notifications module.
 * See line 1199 of notifications.module
 * @param  object $account user account
 * @return array
 */

  if (!function_exists('notifications_queue_send_intervals')) {
    function notifications_queue_send_intervals() {
      return array(
           '0' => t('Immediately'),
           '300' => t('Each 5 minutes'),
           '600' => t('Each 10 minutes'),
           '3600' => t('Every hour'),
           '43200' => t('Twice a day'),
         );
    }
  }

/**
 *
 * Implementation of hook_action_info
 * @return
 */
function notifications_lite_action_info() {
  return array(
    'notifications_lite_interval_action' => array(
      'label' => t('(notifications_lite) Send scheduled notifications'),
      'type' => 'system',
      'configurable' => TRUE,
      'triggers' => array('any'),
    ),
  );
}

/**
 * Notifications builder
 * Filter users according to an interval
 * Un alert is a set of nodes whose interval < 86400
 * Un digest is a set of nodes whose interval >= 86400
 * if interval <  86400 builds alerts (merge all nodes coming from several suscriptions owned by the same user according to the interval defined in form action)
 * if interval >= 86400 builds digests (merge all nodes coming from several suscriptions owned by the same user regardless individuals intervals. These nodes will be sent using the interval defined in digest mode tab)
 * @param  object $entity
 * @param  array  $context
 * @return
 */
function notifications_lite_interval_action(&$entity, $context = array()) {
  $items = array();
  $mode = '';
  $notifications = array();
  $subscriptions = array();
  $interval = $context['interval'];
  $t1 = time() - $interval;
  $t2 = time();

  // simple alert mode (includes interval = 0 for immediate dispatch)
  if ($interval < 86400) {
    $users = notifications_lite_subscriptions_user_load('ALL', $interval);
    $mode = 'alert';
  }
  // digest mode
  elseif ($interval >= 86400) {
    $users = notifications_lite_digest_user_load('ALL', $interval);
    $mode = 'digest';
  }
  foreach ($users as $user) {
    // full_html is default value for alerts, only is possible choose format whether use digest mode
    // this is because for alert mode we are creating a set de nodes coming from several subscriptions with the same interval  and is imposible to determine which format will use
    $format = ($interval < 86400) ? 'full_html' : $user->format;
    $subscriptions = notifications_lite_subscriptions_load('ALL', $user->uid);
    // reset notifications array by each user
    $notifications = array();
    foreach ($subscriptions as $subscription) {
      // add information about subscription to subscription object
      $subscription->subscription = array_combine(explode('|', $subscription->stypes), explode('|', $subscription->svalues));
      $subscription->mode = $mode;
      $subscription->format = $format;
      $items = _notifications_lite_get_items($subscription, $t1, $t2);
      foreach ($items as $item) {
        // to see all subscriptions
        // $notifications[] = $item;
        // to dedupe comments (thread, use node:nid) or nodes
        if ($subscription->stypes == 'node:nid') {
        $notifications['comments'][$item->cid] = $item;
        }
        else {
        $notifications['nodes'][$item->nid] = $item;
        }

      }
    }
      if (!empty($notifications)) {
      _notifications_lite_send_mails($notifications);
      }
      else {
        return;
      }
  }

}

/**
 * Implementation of a form for an Action
 * @param  array $context Action context
 * @return array form
 */
function notifications_lite_interval_action_form($context) {
  $options = notifications_queue_send_intervals() + _notifications_lite_digest_interval();
  $form['interval'] = array(
    '#title' => t('Intervals'),
    '#type' => 'select',
    '#description' => t('Choose an interval'),
    '#options' => $options,
    '#default_value' => isset($context['interval']) ? $context['interval'] : '',
  );
  return $form;
}

/**
 * Implementation of a form for an Action
 * @param  array $form
 * @param  array $form_state
 * @return array
 */
function notifications_lite_interval_action_submit($form, $form_state) {
  return array('interval' => $form_state['values']['interval']);
}

/**
 * Retrieve enabled users form table notifications_subscription for alerts
 * @param  string $uid      user uid to select one user or ALL to select all users
 * @param  integer $interval time interval
 * @return array enabled users list
 */
function notifications_lite_subscriptions_user_load($uid = 'ALL', $interval = NULL) {
  $query = db_select('notifications_subscription', 's');
  $query->fields('s');
  $query->condition('s.status', 1);
  if (!is_null($interval) && is_numeric($interval)) {
    $query->condition('s.send_interval', $interval);
  }
  if (($uid !== 'ALL') && is_numeric($uid)) {
    $query->condition('s.uid', $uid);
  }
  $query->join('users', 'u', 's.uid = u.uid');
  $query->fields('u', array('name', 'mail'));
  // user must be active
  $query->condition('u.status', 1);
  $users = $query->execute()->fetchAllAssoc('uid');
  return $users;
}


/**
 * Retrieve enabled users form table notifications_lite_digest for digest
 * @param  string $uid user uid to select one user or ALL to select all users
 * @param  integer $interval $interval time interval
 * @return array enabled users list
 */
function notifications_lite_digest_user_load($uid = 'ALL', $interval = NULL) {
  $query = db_select('notifications_lite_digest', 'd');
  $query->fields('d');
  $query->condition('d.status', 1);
  if (!is_null($interval) && is_numeric($interval)) {
    $query->condition('d.send_interval', $interval);
  }
  if (($uid !== 'ALL') && is_numeric($uid)) {
    $query->condition('d.uid', $uid);
  }
  $query->join('users', 'u', 'd.uid = u.uid');
  $query->fields('u', array('name', 'mail'));
  // user must be active
  $query->condition('u.status', 1);
  $users = $query->execute()->fetchAllAssoc('uid');
  return $users;
}

/**
 * Load all subscriptions
 * @param  string $sid      subscription id
 * @param  string $uid      user uid
 * @param  integer $interval time interval
 * @param  integer $status   enabled or disabled
 * @return array enabled suscriptions list
 */
function notifications_lite_subscriptions_load($sid = 'ALL', $uid = 'ALL', $interval = NULL, $status = NULL) {
  $query = db_select('notifications_subscription', 's');
  $query->fields('s', array('sid', 'uid', 'send_interval', 'language', 'send_method', 'cron', 'module', 'status', 'created', 'updated'));
  if (($sid !== 'ALL') && is_numeric($sid)) {
  $query->condition('s.sid', $sid);
  }
  if (!is_null($interval) && is_numeric($interval)) {
  $query->condition('s.send_interval', $interval);
  }
  if (($uid !== 'ALL') && is_numeric($uid)) {
  $query->condition('s.uid', $uid);
  }
  if (!is_null($interval) && is_numeric($status)) {
  // whether user active
  $query->condition('s.status', 1);
  }
  $query->join('notifications_subscription_fields', 'f', 's.sid = f.sid');
  $query->fields('f', array('sid'));
  $query->join('users', 'u', 'u.uid = s.uid');
  $query->fields('u', array('mail'));
  $query->addExpression('GROUP_CONCAT(DISTINCT(f.intval) SEPARATOR \'|\')', 'sintvalues');
  $query->addExpression('GROUP_CONCAT(DISTINCT(f.type) SEPARATOR \'|\')', 'stypes');
  // not use values as string because it seems reserved
  $query->addExpression('GROUP_CONCAT(DISTINCT(f.value) SEPARATOR \'|\')', 'svalues');
  $query->groupBy('s.sid');
  $subscriptions = $query->execute()->fetchAllAssoc('sid');
  return $subscriptions;
}

/**
 * Put a array of messages by user into a queue
 * @param  array  $notifications array of messages
 * @return
 */
function _notifications_lite_send_mails($notifications = array()) {
  $queue = DrupalQueue::get('notifications_lite');
  $queue->createItem($notifications);
  return;
}

/**
 * Return an array of items according type of subscription
 * @param  object $subscription subscription (a row of table notifications_subscription)
 * @param  integer $t1 interval beginning
 * @param  integer $t2 interval ending
 * @return array of nodes
 */
function _notifications_lite_get_items($subscription, $t1, $t2) {
  $items = array();
  $type = $subscription->stypes;
  switch ($type) {
    case 'node:type':
      $items = _notifications_build_query_node($subscription, $t1, $t2);
      break;
    case 'term:tid':
      $items = _notifications_build_query_taxonomy($subscription, $t1, $t2);
      break;
    case 'user:uid':
      $items = _notifications_build_query_user($subscription, $t1, $t2);
      break;
    case 'node:type|user:uid':
      $items = _notifications_build_query_user_content_type($subscription, $t1, $t2);
      break;
    case 'node:nid':
      $items = _notifications_build_query_content_thread($subscription, $t1, $t2);
      break;
    default:
      # code...
      break;
  }
  return $items;
}

/**
 * Return an array of comments filter by thread
 * @param  [type] $subscription subscription
 * @param  [type] $t1           interval beginning
 * @param  [type] $t2           interval ending
 * @return array of comments
 */
function _notifications_build_query_content_thread($subscription, $t1, $t2) {
  $items = array();
  $rows = array();
  $nid = $subscription->subscription['node:nid'];
  $recipient = $subscription->mail;
  $query = db_select('comment', 'c');
  $query->fields('c');
  $query->condition('c.created', array($t1, $t2) , 'BETWEEN');
  $query->condition('c.nid',$nid);
  $rows = $query->execute();
  foreach ($rows as $row) {
    $row->recipient = $recipient;
    $row->subscription = $subscription;
    $items[] = $row;
  }
  return $items;
}


/**
 * Return an array of nodes filter by node type
 * @param  [type] $subscription subscription
 * @param  [type] $t1           interval beginning
 * @param  [type] $t2           interval ending
 * @return array of nodes
 */
function _notifications_build_query_node($subscription, $t1, $t2) {
  $items = array();
  $rows = array();
  $nodetype = $subscription->subscription['node:type'];
  $recipient = $subscription->mail;
  $query = db_select('node', 'n');
  $query->fields('n');
  $query->condition('n.created', array($t1, $t2) , 'BETWEEN');
  $query->condition('n.type', $nodetype);
  $query->join('field_data_body', 'd', 'd.entity_id = n.nid');
  $query->condition('d.entity_type', 'node');
  $query->fields('d', array('body_value', 'body_format'));
  $rows = $query->execute();
  foreach ($rows as $row) {
    $row->recipient = $recipient;
    $row->subscription = $subscription;
    $items[] = $row;
  }
  return $items;
}

/**
 * Return an array of nodes filter by term id
 * @param  [type] $subscription subscription
 * @param  [type] $t1           interval beginning
 * @param  [type] $t2           interval ending
 * @return array of nodes
 */
function _notifications_build_query_taxonomy($subscription, $t1, $t2) {
  $items = array();
  $rows = array();
  $tid = $subscription->subscription['term:tid'];
  $recipient = $subscription->mail;
  $query = db_select('taxonomy_index', 't');
  $query->fields('t', array('nid'));
  $query->condition('t.tid', $tid);
  $query->join('node', 'n', 'n.nid = t.nid');
  $query->condition('n.created', array($t1, $t2) , 'BETWEEN');
  $query->fields('n');
  $query->join('field_data_body', 'd', 'd.entity_id = n.nid');
  $query->condition('d.entity_type', 'node');
  $query->fields('d', array('body_value', 'body_format'));
  $rows = $query->execute();
  foreach ($rows as $row) {
    $row->recipient = $recipient;
    $row->subscription = $subscription;
    $items[] = $row;
  }
  return $items;
}

/**
 * Return an array of nodes filter by user id
 * @param  [type] $subscription subscription
 * @param  [type] $t1           interval beginning
 * @param  [type] $t2           interval ending
 * @return array of nodes
 */

function _notifications_build_query_user($subscription, $t1, $t2) {
  $items = array();
  $rows = array();
  $uid = $subscription->subscription['user:uid'];
  $recipient = $subscription->mail;
  $query = db_select('node', 'n');
  $query->fields('n');
  $query->condition('n.created', array($t1, $t2) , 'BETWEEN');
  $query->condition('n.uid', $uid);
  $query->join('field_data_body', 'd', 'd.entity_id = n.nid');
  $query->condition('d.entity_type', 'node');
  $query->fields('d', array('body_value', 'body_format'));
  $rows = $query->execute();
  foreach ($rows as $row) {
    $row->recipient = $recipient;
    $row->subscription = $subscription;
    $items[] = $row;
  }
  return $items;
}

/**
 * Return an array of nodes filter by user id and  node type
 * @param  [type] $subscription subscription
 * @param  [type] $t1           interval beginning
 * @param  [type] $t2           interval ending
 * @return array of nodes
 */

function _notifications_build_query_user_content_type($subscription, $t1, $t2) {
  $items = array();
  $rows = array();
  $uid = $subscription->svalues;
  $recipient = $subscription->mail;
  $uid = $subscription->subscription['user:uid'];
  $nodetype = $subscription->subscription['node:type'];
  $query = db_select('node', 'n');
  $query->fields('n');
  $query->condition('n.created', array($t1, $t2) , 'BETWEEN');
  $query->condition('n.uid', $uid);
  $query->condition('n.type', $nodetype);
  $query->join('field_data_body', 'd', 'd.entity_id = n.nid');
  $query->condition('d.entity_type', 'node');
  $query->fields('d', array('body_value', 'body_format'));
  $rows = $query->execute();
  foreach ($rows as $row) {
    $row->recipient = $recipient;
    $row->subscription = $subscription;
    $items[] = $row;
  }
  return $items;
}


/**
 * Return an array of nodes filter by user id to build a digest
 * @param  [type] $subscription subscription
 * @param  [type] $t1           interval beginning
 * @param  [type] $t2           interval ending
 * @return array of nodes
 */
function _notifications_build_query_digest($subscription, $t1, $t2) {
  $items = array();
  $rows = array();
  $uid = $subscription->subscription['user:uid'];
  $recipient = $subscription->mail;
  $query = db_select('node', 'n');
  $query->fields('n');
  $query->condition('n.created', array($t1, $t2) , 'BETWEEN');
  $query->condition('n.uid', $uid);
  $query->join('field_data_body', 'd', 'd.entity_id = n.nid');
  $query->condition('d.entity_type', 'node');
  $query->fields('d', array('body_value', 'body_format'));
  $rows = $query->execute();
  foreach ($rows as $row) {
    $row->recipient = $recipient;
    $row->subscription = $subscription;
    $items[] = $row;
  }
  return $items;
}

/**
 * Implementation of hook_cron_queue_info
 * @return queue of messages
 */
function notifications_lite_cron_queue_info() {
  $queues = array();
  $queues['notifications_lite'] = array(
    'worker callback' => '_notifications_lite_sendmail', //function to call for each item
    'time' => 60, //seconds to spend working on the queue
  );
  return $queues;
}

/**
 * Defines digest intervals
 * @return array of intervals
 */
function _notifications_lite_digest_interval() {
  return array(
    '86400' => t('Daily (digest)'),
    '604800' => t('Weekly (digest)'),
    '2592000' => t('Monthly (digest)'),
  );
}

/**
 * Implementation of views API
 * @return array
 */
function notifications_lite_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'notifications_lite') . '/includes',
  );
}



